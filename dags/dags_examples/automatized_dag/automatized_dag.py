import airflow
from airflow import DAG
from scripts.lib.biolab_nodes import BiolabNodes
from scripts.lib.biolab_functions import read_config

dag_name = "automatized_dag"
# ------------------------- INITIAL DAG ----------------------------------
with DAG(
        dag_id=dag_name,
        description="First approach  to mix airflow and pyfoomb using Docker. Automatized Example",
        start_date=airflow.utils.dates.days_ago(1),
        schedule_interval=None,
) as dag:
    # Instance of the class with the path of the project
    # You must change the path for your own path
    Experiment = BiolabNodes(path="C:/Users/Fede/Documents/Airflow/biolab-airflow")

    # -------------- Initialize parameters, initial values and times form config.cfg file ---------------------------
    mbrs_row = ["mbr{}".format(row) for row in range(1, read_config('MBR', dag_name)['QTY_R'] + 1)]
    mbrs_col = ["{}".format(col) for col in range(1, read_config('MBR', dag_name)['QTY_C'] + 1)]
    unknows_estimate = read_config('PARAMETER_ESTIMATION_BOUNDS', dag_name)
    unknows_optimization = read_config('PARAMETER_OPTIMIZATION_BOUNDS', dag_name)

    # Extract modify params values
    modify_params = read_config('MODIFY_PARAMS', dag_name)

    # sample time between mbr - parameter estimation time (duration of the process)
    ts = read_config('TIMES', dag_name)['TS']
    tp = read_config('TIMES', dag_name)['TP']

    # Get sample points (times) from config file
    sample_time = read_config('SAMPLE_TIME', dag_name)['SP']

    # create bioreactor name row + column
    mbrs = []
    for i in mbrs_row:
        for j in mbrs_col:
            mbrs.append(i + j)

    # -------------------------- DAGs Start Node -------------------------------
    start = Experiment.start(
        task_id="Start",
        dag_id=dag_name
    )

    node = {}

    # for each sample defined, do an init/update - sample - parameter estimation - parameter optimization LOOP
    for sample, t_sample in enumerate(sample_time, start=1):

        for index_c, c in enumerate(mbrs_col):
            # --------------------------- update states with old parameters --------------------------
            for index_r, mbr in enumerate(mbrs_row):
                # in the case of first sample, the current time is 0, in other case is:
                # last sample time + time between mbr sample (ts) for each mbr +
                # parameter estimation time (tp) for each sample
                current_update_time = 0 if sample == 1 else sample_time[sample - 2] + (ts * len(mbrs_col)) + tp

                if sample == 1:
                    node["Init_{}.{}".format(mbr, c)] = Experiment.init_mbr(
                        task_id="Init_{}.{}".format(mbr, c),
                        mbr=mbr + c,
                        dag_id=dag_name
                    )

                # ------------------------- simulate with new parameters and currents states -----------------

                node["Sample_{}_{}.{}".format(sample, mbr, c)] = Experiment.sample(
                    task_id="Sample_{}_{}.{}".format(sample, mbr, c),
                    modify_param=modify_params[index_r][index_c],
                    tf="{}".format(t_sample + ts * index_c),
                    mbr="{}".format(mbr + c)
                )

        # ---------------- estimate parameters with news states and model parameter---------------

        node["Parameter_update_{}".format(sample)] = Experiment.parameter_distribution_update(
            task_id="Parameter_update_{}".format(sample),
            mbrs=mbrs,
            unknows={},
            modify_params=modify_params
        )

        # ------------------------ Parameter optimization -----------------------------------------------

        node["Online_redesign_{}".format(sample)] = Experiment.bayesian_online_redesign(
            task_id="Online_redesign_{}".format(sample),
            mbrs=mbrs,
            unknows=unknows_optimization,
            # tf="{}".format(t_sample + tp + ts * len(mbrs_row))
            tf="{}".format(sample_time[-1])
        )

        # ------------------------------------Dependencies------------------------------------------------
        for index, c in enumerate(mbrs_col):
            for mbr in mbrs_row:
                if sample == 1:
                    start >> node["Init_{}.{}".format(mbr, c)] >> \
                    node["Sample_{}_{}.{}".format(1, mbr, c)]
                elif index == 0:
                    last_node >> node["Sample_{}_{}.{}".format(sample, mbr, c)]

                # The first MBR has no sampling dependency
                if index:
                    node["Sample_{}_{}.{}".format(sample, mbr, mbrs_col[index - 1])] >> \
                    node["Sample_{}_{}.{}".format(sample, mbr, c)]

                # The parameter estimation depends on the last sample
                if index == len(mbrs_col) - 1:
                    node["Sample_{}_{}.{}".format(sample, mbr, c)] >> \
                    node["Parameter_update_{}".format(sample)] >> \
                    node["Online_redesign_{}".format(sample)]

        # Set the last node as online redesign
        last_node = node["Online_redesign_{}".format(sample)]

    # ------------------------ Show results on plot -----------------------------------------------
    # Run simulation with new parameters and new policy to compare results
    Show_results = Experiment.show_results(
        task_id="Show_results",
        tf="{}".format(sample_time[-1]),
        mbrs=mbrs,
        unknows={}
    )

    last_node >> Show_results

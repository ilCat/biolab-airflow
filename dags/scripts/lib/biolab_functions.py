import logging
from pathlib import Path
import os
import json

logging.basicConfig(level=logging.INFO)

# ------------------------ NEW JSON FUNCTIONS ---------------------#

measurements_file = "../../data/samples/{}.json"
params_file = "../../data/params/params.json"
config_file = "../../dags_examples/{}/config.json"


def read_config(item, dag_id):
    _dir = os.path.join(os.path.dirname(__file__), config_file.format(dag_id))
    with open(_dir) as file:
        data = json.load(file)
    return data[item]


# TODO: rename
def calc_times(dag_id):
    # Delta time between pulses of feed, equal for all MBRs
    delta_t_feed = read_config('FEED', dag_id)['DELTA_TIME']
    # Get number of samples from config file
    sample_number = read_config('SAMPLES', dag_id)['QUANTITY']
    # Batch Time
    tf = read_config('MODEL_PARAMETERS_PRIORI', dag_id)['tF']
    # Create of times Feed and times Samples
    t_pulse = []
    t_sample = []
    # Time between Feed pulse and Sample given for the block1, It's 3/10 of delta_t_Feed
    # 3/10: 3 block tasks over 10
    t_between_fs = delta_t_feed * 3 / 10
    # The delta time between samples is equivalent to 2 times of feed. It mean, each 2 feeds 1 sample is done
    delta_t_sample = delta_t_feed * 2
    for i in range(int(sample_number)):
        # The first elements are affected by tF (batch time), the rest of the times just add delta times
        t_pulse.append(tf) if i == 0 else t_pulse.append(t_pulse[i - 1] + delta_t_feed)
        t_sample.append(t_pulse[i] + delta_t_feed + t_between_fs) if i == 0 else t_sample.append(
            t_sample[i - 1] + delta_t_sample)
    return t_pulse, t_sample


def init_params(dag_id):
    # Read config file
    params = {
        'parameters_estimated': [read_config('MODEL_PARAMETERS_PRIORI', dag_id)],
        'parameters_real': read_config('MODEL_PARAMETERS_REAL', dag_id)
    }

    # Write output.
    output_path = check_path(params_file)
    logging.info("Writing to %s", output_path)
    with open(output_path, "w+", newline='') as file:
        json.dump(params, file, ensure_ascii=False, indent=2)


def init_measurements(mbr, dag_id):
    # Read config file
    measurement = {}
    initial_measurement = read_config('INITIAL_STATES', dag_id)

    for k, v in initial_measurement.items():
        measurement[k[:-1]] = v

    json_data = {'samples': [measurement]}

    # Write output
    output_path = check_path(measurements_file.format(mbr))
    logging.info("Writing to %s", output_path)
    with open(output_path, "w+", newline='') as file:
        json.dump(json_data, file, ensure_ascii=False, indent=2)


def get_measurements(mbr):
    file_path = os.path.join(os.path.dirname(__file__), measurements_file.format(mbr))
    with open(file_path, 'r', encoding='utf-8') as f:
        json_content = json.load(f)
    return json_content


def get_params():
    file_path = os.path.join(os.path.dirname(__file__), params_file)
    with open(file_path, 'r', encoding='utf-8') as f:
        json_content = json.load(f)
    return json_content


def save_measurements(mbr, measurements):
    output_path = check_path(measurements_file.format(mbr))
    logging.info("Writing to %s", output_path)
    with open(output_path, 'w+', encoding='utf-8') as f:
        json.dump(measurements, f, ensure_ascii=False, indent=2)


def save_params(params):
    output_path = check_path(params_file)
    logging.info("Writing to %s", output_path)
    with open(output_path, 'w+', encoding='utf-8') as f:
        json.dump(params, f, ensure_ascii=False, indent=2)


def check_path(_dir):
    file = os.path.join(os.path.dirname(__file__), _dir)
    output_path = Path(file)
    output_dir = output_path.parent
    output_dir.mkdir(parents=True, exist_ok=True)
    return output_path

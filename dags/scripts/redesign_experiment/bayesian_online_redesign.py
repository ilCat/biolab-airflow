import logging
import numpy as np
import click
import json
import sys
from bayes_opt import BayesianOptimization

# to import the library biolab_functions
sys.path.insert(0, '../../..')
from scripts.models.assimulo.model_execution import ExponentialFedBatch
from scripts.parameter_update.variational_inference import VariationalInference
from scripts.lib.biolab_functions import get_params, get_measurements, save_params

logging.basicConfig(level=logging.INFO)


def bayesian_optimization(initial_values, model_parameters, unknows, tf):
    # objetive: maximize biomass concentration
    def objective_function(**params):

        # check if model parameters are distributions or determined
        if isinstance(list(model_parameters.values())[0], list):
            svi = VariationalInference(prior=model_parameters)
            params_sample = svi.sample_param()

        for param in params:
            params_sample[param] = params[param]

        logging.info("Running estimated simulation...")
        x0 = [v for k, v in initial_values.items() if k != 'TP']
        try:
            states = ExponentialFedBatch().execute(x0=x0, t=[0, tf], model_parameters=params_sample,
                                                   initial_values=initial_values)
        except:
            logging.info("[Bayesian Optimization] - Error on model execution - continue")
            return 0
        return states[1][-1][0]

    optimizer = BayesianOptimization(
        f=objective_function,
        pbounds=unknows,
        random_state=1,
        verbose=2
    )

    logging.info("Optimization ready")
    optimizer.maximize(
        init_points=2,
        n_iter=12,
        acq="ucb",
        kappa=3
    )

    logging.info("Optimizer MAX: {}".format(optimizer.max))

    return optimizer.max["params"]


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameter to optimize and bounds",
)
@click.option(
    "--tf",
    type=click.STRING,
    required=True,
    help="Final time",
)
@click.option(
    "--seed",
    type=click.INT,
    required=False,
    help="Seed for random number generator",
)
def main(mbrs, unknows, tf, seed):
    # Set seed for random number generator
    np.random.seed(seed)

    logging.info("Initializing bioreactor parameters")
    MBRs = json.loads(mbrs)
    _unknows = json.loads(unknows)
    model_parameters = get_params()
    measurements = get_measurements(MBRs[0])

    logging.info("Initializing optimization")
    optimal_params = bayesian_optimization(
        initial_values=measurements['samples'][0],
        model_parameters=model_parameters['parameters_estimated'][-1],
        unknows=_unknows,
        tf=float(tf)
    )

    logging.info("Optimization Finished")

    # fix decimal length
    optimal_params = {k: round(v, 3) for k, v in optimal_params.items()}

    # replace new estimations
    model_parameters['parameters_estimated'][-1].update(optimal_params.items())

    # save new params
    save_params(model_parameters)


if __name__ == "__main__":
    main()

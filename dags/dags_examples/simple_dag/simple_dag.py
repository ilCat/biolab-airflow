import airflow
from airflow import DAG
from scripts.lib.biolab_functions import read_config
from scripts.lib.biolab_nodes import BiolabNodes

dag_name = "simple_dag"
with DAG(
        dag_id=dag_name,
        description="First approach  to mix airflow and pyfoomb using Docker. Simple Example.",
        start_date=airflow.utils.dates.days_ago(1),
        schedule_interval=None,
) as dag:
    # Instance of the class with the path of the project. You must change the path for your own path
    Experiment = BiolabNodes(path="C:/Users/Fede/Documents/Airflow/biolab-airflow")

    # --------------------------------------- DAGs Start Node -----------------------------------------------------
    start = Experiment.start(
        task_id="start",
        dag_id=dag_name
    )
    # -------------------------------- Initialice  initial values form config.cfg ---------------------------------
    Init_mbr = Experiment.init_mbr(
        task_id="Init_mbr",
        mbr="mbr",
        dag_id=dag_name
    )
    # ------------------------------ Real model Samples and prior model simulation   ------------------------------
    modify_param = read_config("MODIFY_PARAMS", dag_name)
    Sample_1 = Experiment.sample(
        task_id="Sample_1",
        modify_param=modify_param,
        tf="3.0",
        mbr="mbr"
    )
    Sample_2 = Experiment.sample(
        task_id="Sample_2",
        modify_param=modify_param,
        tf="6.0",
        mbr="mbr"
    )
    Sample_3 = Experiment.sample(
        task_id="Sample_3",
        modify_param=modify_param,
        tf="12.0",
        mbr="mbr"
    )
    # ---------------------------------------- Parameter estimation -----------------------------------------------
    unknows_est = read_config("PARAMETER_ESTIMATION_BOUNDS", dag_name)
    Parameter_update = Experiment.parameter_update(
        task_id="Parameter_update",
        mbrs=["mbr"],
        unknows=unknows_est
    )
    # # -------------------------------------- Parameter optimization -----------------------------------------------
    # unknows_opt = read_config("PARAMETER_OPTIMIZATION_BOUNDS", dag_name)
    # Online_redesign = Experiment.bayesian_online_redesign(
    #     task_id="Online_redesign",
    #     mbrs=["mbr"],
    #     unknows=unknows_opt,
    #     tf="15.0"
    # )
    # -------------------------------- Update states with old parameters-------------------------------------------
    show_result = Experiment.show_results(
        task_id="show_result",
        tf="25.0",
        mbrs=["mbr"],
        unknows={}
    )
    # Update_mbr = Experiment.sample(
    #     task_id="Update_mbr",
    #     modify_param=modify_param,
    #     tf="15.0",
    #     mbr="mbr"
    # )
    # # ------------------------ Real model Samples and prior model simulation   -------------------------------------
    # Sample_4 = Experiment.sample(
    #     task_id="Sample_4",
    #     modify_param={'mu_set': 1.01},
    #     tf="20.0",
    #     mbr="mbr"
    # )
    # --------------------------------------------Dependencies-----------------------------------------------------
    start >> Init_mbr >> Sample_1
    Sample_1 >> Sample_2 >> Sample_3
    Sample_3 >> Parameter_update >> show_result

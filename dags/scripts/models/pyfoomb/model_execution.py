import logging
import click
import numpy as np
import json
from pyfoomb import BioprocessModel
from pyfoomb import Caretaker
from pyfoomb import Helpers
import sys
from scipy import signal

# to import the library biolab_functions
sys.path.insert(0, '../../..')
from scripts.lib.biolab_functions import get_params, get_measurements, save_measurements

logging.basicConfig(level=logging.INFO)


# Defines the model class
class ExponentialFedBatch(BioprocessModel):

    def __init__(self, model_parameters: list, states: list, initial_switches: list = None, model_name: str = None,
                 replicate_id: str = None):

        # Call BioprocessModel init
        super(ExponentialFedBatch, self).__init__(model_parameters, states, initial_switches, model_name, replicate_id)

        # Call Assimulo's ExplicitProblem init, to override BioprocessModel init, adding time "t0"
        super(BioprocessModel, self).__init__(
            t0=helper.get_t0(),
            y0=self.initial_values.to_numpy(),
            sw0=self.initial_switches,
            name=self._name,
        )

    def rhs(self, t, y, sw):

        # Unpacks the state vector. The states are alphabetically ordered.
        P, S, VL, X = y

        # Unpacks the model parameters.
        YPX = self.model_parameters['YPX']
        YXS = self.model_parameters['YXS']
        cSF = self.model_parameters['cSF']
        mu_set = self.model_parameters['mu_set']
        tF = self.model_parameters['tF']

        # For calculation of F, these two initial values are needed
        # S0 = get_initial_states()['S0']
        # VL0 = get_initial_states()['VL0']
        # TODO: obtener del archivo de configuracion, sin afectar la optimizacion del experimento (pygmo)
        S0 = 40
        VL0 = 1.0

        # Calculate the current specific rates
        mu = self.growth_rate(y)
        qS = 1 / YXS * mu
        qP = YPX * mu

        # Calculate the feeding profile, conditional to the corresponding events
        if sw[1] and not sw[0]:
            scuare = (1 + signal.square(2*np.pi/5*(t-tF) , 0.1)) /2
            profile = (S0 * VL0 * mu_set) / (cSF - S) * np.exp(mu_set * (t - tF))
            F = scuare * profile
        else:
            F = 0.0

        # Calculate state derivatives
        dXdt = mu * X - F / VL * X
        dSdt = -qS * X + F / VL * (cSF - S)
        dPdt = qP * X - F / VL * P
        dVLdt = F

        # Return list of state derivatives in the same order as the state vector was unpacked
        return [dPdt, dSdt, dVLdt, dXdt]

    def state_events(self, t, y, sw):

        P, S, VL, X = y
        VL_max = self.model_parameters['VL_max']
        # The event is hit, when this expression evaluates to zero
        event_VL_max = VL - VL_max

        tF = self.model_parameters['tF']
        # The event is hit, when this expression evaluates to zero
        event_tF = t - tF
        return [event_VL_max, event_tF]

    # The Monod equation is defined as instance method
    def growth_rate(self, y):
        P, S, VL, X = y
        mu_max = self.model_parameters['mu_max']
        kS = self.model_parameters['kS']
        mu = mu_max * S / (kS + S)
        return mu


# Helper class to fix initial time (t0) on Pyfoomb
class HelperPyfoomb:
    def __init__(self, _t0=0):
        self.t0 = _t0

    def set_t0(self, _t0):
        self.t0 = _t0

    def get_t0(self):
        return self.t0


helper = HelperPyfoomb()


@click.command()
@click.option(
    "--mbr",
    type=click.STRING,
    required=True,
    help="Initial values",
)
@click.option(
    "--modify",
    type=click.STRING,
    required=True,
    help="Parameters modifier",
)
@click.option(
    "--t0",
    type=click.FLOAT,
    required=True,
    help="Initial execution time",
)
@click.option(
    "--tf",
    type=click.FLOAT,
    required=True,
    help="Final execution time",
)
@click.option(
    "--seed",
    type=click.INT,
    required=False,
    help="Seed for random number generator",
)
def main(mbr, modify, t0, tf, seed):
    # Set seed for random number generator
    np.random.seed(seed)

    logging.info("Initializing bioreactor parameters")
    model_parameters = get_params()
    measurements = get_measurements(mbr)
    measurements_name = measurements['samples'][0].keys()
    modify_param = json.loads(modify)

    # Check if t0 == tf is an UPDATE, else is a SAMPLE
    if t0 == tf:
        t0 = measurements['simulations'][-1]['TP']

    logging.info("modify coefficient:  %s", modify_param)
    logging.info("t0:  %s", t0)
    logging.info("tf:  %s", tf)

    helper.set_t0(t0)

    # -------------------------------------------------------------------------------------

    # Modify params (apply coeff)
    for param in modify_param:
        model_parameters['parameters_estimated'][-1][param] = model_parameters['parameters_estimated'][-1][param] * \
                                                              modify_param[param]

    logging.info("Running estimated simulation...")

    caretaker_estimated = Caretaker(
        bioprocess_model_class=ExponentialFedBatch,
        model_parameters=model_parameters['parameters_real'][-1],
        initial_values=measurements['simulations'][0]
    )
    logging.info("t0:  %s", t0)
    logging.info("tf:  %s", tf)
    # logging.info("Running estimated simulation...")
    simulation_estimated = caretaker_estimated.simulate(t=np.linspace(t0, tf, 30))

    # Extract state result and timepoints
    states_estimated = {}

    for state in measurements_name:
        if state != 'TP':
            states_estimated[state] = Helpers.extract_time_series(simulation_estimated, name=state.upper(),
                                                                  replicate_id=None).values

    states_estimated['TP'] = Helpers.extract_time_series(simulation_estimated, name=measurements_name[0],
                                                         replicate_id=None).timepoints

    # TODO: hacer el guardado


if __name__ == "__main__":
    main()

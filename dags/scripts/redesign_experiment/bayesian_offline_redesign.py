import logging
import numpy
from pyfoomb import Helpers, Caretaker, Visualization
from bayes_opt import BayesianOptimization
import sys

# to import the library biolab_functions
sys.path.insert(0, '../..')

from scripts.models.pyfoomb.model_execution import ExponentialFedBatch

logging.basicConfig(level=logging.INFO)


def bayesian_optimization(initial_values, tf):
    # objetive: maximize biomass concentration
    def objective_function(tfeed, mu_set):
        caretaker = Caretaker(
            bioprocess_model_class=ExponentialFedBatch,
            model_parameters={
                'kS': 0.02,
                'mu_max': 0.3,
                'YXS': 0.3,
                'YPX': 0.35,
                'cSF': 500.0,
                'mu_set': mu_set,
                'tF': tfeed,
                'VL_max': 2.5,
            },
            initial_values=initial_values
        )
        result = caretaker.simulate(t=numpy.linspace(0, tf, 30))

        P_values = Helpers.extract_time_series(result, name='P', replicate_id=None).values

        return P_values[-1]

    pbounds = {
        'tfeed': (0, 10),
        'mu_set': (0, 5),
    }

    optimizer = BayesianOptimization(
        f=objective_function,
        pbounds=pbounds,
        random_state=1,
        verbose=2
    )

    optimizer.maximize(
        init_points=2,
        n_iter=15,
        acq="ucb",
        kappa=2
    )

    logging.info("Optimizer MAX: {}".format(optimizer.max))

    best_caretaker = Caretaker(
        bioprocess_model_class=ExponentialFedBatch,
        model_parameters={
            'kS': 0.02,
            'mu_max': 0.4,
            'YXS': 0.5,
            'YPX': 0.2,
            'cSF': 500.0,
            'mu_set': optimizer.max["params"]["mu_set"],
            'tF': optimizer.max["params"]["tfeed"],
            'VL_max': 2.5,
        },
        initial_values=initial_values
    )
    result = best_caretaker.simulate(t=tf)

    Visualization.show_kinetic_data(result, ncols=2, savename="optimized.png")

    return 1


def main():
    initial_values = {
        'S0': 40.0,
        'X0': 0.1,
        'P0': 0.0,
        'VL0': 1.0,
    }

    bayesian_optimization(
        initial_values=initial_values,
        tf=48.0
    )


if __name__ == "__main__":
    main()

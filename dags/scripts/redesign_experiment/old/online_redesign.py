import pyfoomb
import multiprocessing
import numpy
import pygmo
import logging
import click
import json
import csv
import sys
# to import the library biolab_functions and model_execution
sys.path.insert(0, '../../..')
from pyfoomb import Caretaker
from pyfoomb import Helpers
from pyfoomb.datatypes import TimeSeries
from IPython.utils import io
from scripts.models.pyfoomb.model_execution import ExponentialFedBatch
from scripts.lib.biolab_functions import get_param, get_initial_values
from pathlib import Path

# Sets random number generator
numpy.random.seed(1234)
# print(f'Current package version of {pyfoomb.__name__}: {pyfoomb.__version__}')
n_cpus = multiprocessing.cpu_count()
# print(f'Current {pygmo.__name__} version: {pygmo.__version__}')


class OptimizationProblem:

    def __init__(self, unknowns: list, bounds: list, tfinal: float, caretaker: Caretaker, target: str):
        self.unknowns = unknowns
        self.lower_bounds = [_bounds[0] for _bounds in bounds]
        self.upper_bounds = [_bounds[1] for _bounds in bounds]
        self.tfinal = tfinal
        self.caretaker = caretaker
        self.target = target

    def fitness(self, x):

        # Create current parameter dictionary
        parameters = {_name: _x for _name, _x in zip(self.unknowns, x)}

        # Run  a forward simulation for the current parameter values
        try:
            simulation = self.caretaker.simulate(t=self.tfinal, parameters=parameters)
            P = Helpers.extract_time_series(simulation, name='P', replicate_id=None).values
            VL = Helpers.extract_time_series(simulation, name='VL', replicate_id=None).values
            t = Helpers.extract_time_series(simulation, name='P', replicate_id=None).timepoints
            _P_mass = P * VL
            P_m = TimeSeries(name="P_m", timepoints=t, values=_P_mass)
            P_max = numpy.max(_P_mass)           
    
            # Assign specified optimization target
            if self.target == 'P_max':
                obj = -1 * P_max

        # Toxic (combination of) parameter values cause integration errors
        except CVodeError:
            print(f'\n\nApparently toxic parameters:\n{parameters}')
            obj = numpy.inf

        # # Violation of this constraint is strongly penalized
        # if parameters['tF1'] > parameters['tF2']:
        #     obj = numpy.inf

        # Must return an array-like variable.
        return [obj]

    def get_bounds(self):
        return (self.lower_bounds, self.upper_bounds)

    @staticmethod
    def extract_results(unknowns, archipelago):
        """
        A static convenience method that extracts the optimized results for making nice output
        """

        estimates_info = {}
        best_idx = numpy.argmin(numpy.array(archipelago.get_champions_f()).flatten())
        best_estimates = {
            _unknown: _x
            for _unknown, _x in zip(unknowns, archipelago[int(best_idx)].get_population().champion_x)
        }
        best_objective = archipelago[int(best_idx)].get_population().champion_f
        return best_estimates, best_objective


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameter to optimize and bounds",
)
@click.option(
    "--tf",
    type=click.STRING,
    required=True,
    help="Final time",
)
def main(mbrs, unknows, tf):
    logging.info("Initializing bioreactor parameters")
    outputs = ['/data/params/optimized_param.csv', '/data/params/real_param.csv']
    MBRs = json.loads(mbrs)
    _unknows = json.loads(unknows)
    model_parameters = get_param('..{}'.format(outputs[0]))
    initial_values = get_initial_values(MBRs[0])

    caretaker = Caretaker(
        bioprocess_model_class=ExponentialFedBatch,
        model_parameters=model_parameters,
        initial_values=initial_values
    )

    # Creates an instance of `OptimizationProblem`, needed for the archipelago creation
    opt_problem = pygmo.problem(
        OptimizationProblem(
            unknowns=list(_unknows.keys()),
            bounds=list(_unknows.values()),
            tfinal=tf,
            caretaker=caretaker,
            target='P_max',
        )
    )
    logging.info("optimization problem ready")

    # Create the archipelago
    # NOTE: May take some time since this process is (here) not parallelized for simplicity
    with io.capture_output() as captured:
        archi = pygmo.archipelago(
            n=n_cpus,
            algo=pygmo.de1220(gen=10),
            prob=opt_problem,
            pop_size=7 * len(_unknows.keys()),
            t=pygmo.fully_connected(),  # could also be: pygmo.ring()
            udi=pygmo.mp_island(),
        )
    archi.wait_check()
    logging.info("Archipielago ready")

    # Run a few rounds of evolution, and report intermediate objective values, as well as the finally optimized parameters
    evolutions = 10

    for i in range(evolutions):
        archi.evolve()
        archi.wait_check()
        best_estimates, best_objective = OptimizationProblem.extract_results(list(_unknows.keys()), archi)
        print(f'Optimal product after {i + 1} evolutions: {-1 * best_objective[0]}')

    print('\nOptimal parameter values\n--------------------------')
    for _p in best_estimates:
        print(f'{_p} : {best_estimates[_p]}')

    for dir in outputs:
        with open(dir, "r+", newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            *_, last = reader
            for name_par, val in last.items():
                # Read dict of parameter from csv
                for k, v in best_estimates.items():
                    # logging.info("Reading %s from result", k)
                    if k == name_par:
                        last[name_par] = v
                logging.info("Writing in  %s the value %s", name_par, last[name_par])
                last[name_par] = float(last[name_par])

        # Write output.
        output_path = Path(dir)
        logging.info("Writing to %s", output_path)
        with open(output_path, "a+", newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_NONE)
            writer.writerow(last.values())


if __name__ == "__main__":
    main()

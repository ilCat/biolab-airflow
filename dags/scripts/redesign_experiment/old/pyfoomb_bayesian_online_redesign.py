import logging
import numpy
import click
import json
import sys
import csv
from pyfoomb import Helpers, Caretaker
from pathlib import Path
from bayes_opt import BayesianOptimization

# to import the library biolab_functions
sys.path.insert(0, '../../..')

from scripts.models.pyfoomb.model_execution import ExponentialFedBatch
from scripts.lib.biolab_functions import get_param, get_initial_values

# # Sets random number generator
numpy.random.seed(1234)

logging.basicConfig(level=logging.INFO)


def bayesian_optimization(initial_values, model_parameters, unknows, tf):
    # objetive: maximize biomass concentration
    def objective_function(**params):
        for param in params:
            model_parameters[param] = params[param]

        caretaker = Caretaker(
            bioprocess_model_class=ExponentialFedBatch,
            model_parameters=model_parameters,
            initial_values=initial_values
        )
        result = caretaker.simulate(t=tf)

        P_values = Helpers.extract_time_series(result, name='P', replicate_id=None).values

        return P_values[-1]

    optimizer = BayesianOptimization(
        f=objective_function,
        pbounds=unknows,
        random_state=1,
        verbose=2
    )

    logging.info("Optimization ready")
    optimizer.maximize(
        init_points=2,
        n_iter=12,
        acq="ucb",
        kappa=3
    )

    logging.info("Optimizer MAX: {}".format(optimizer.max))

    return optimizer.max["params"]


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameter to optimize and bounds",
)
@click.option(
    "--tf",
    type=click.STRING,
    required=True,
    help="Final time",
)
def main(mbrs, unknows, tf):
    logging.info("Initializing bioreactor parameters")
    outputs = ['/data/params/optimized_param.csv', '/data/params/real_param.csv']
    MBRs = json.loads(mbrs)
    _unknows = json.loads(unknows)
    model_parameters = get_param('..{}'.format(outputs[0]))
    initial_values = get_initial_values(MBRs[0])

    logging.info("Initializing optimization")
    optimal_params = bayesian_optimization(
        initial_values=initial_values,
        model_parameters=model_parameters,
        unknows=_unknows,
        tf=float(tf)
    )

    for dir in outputs:
        with open(dir, "r+", newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            *_, last = reader
            for name_par, val in last.items():
                # Read dict of parameter from csv
                for k, v in optimal_params.items():
                    # logging.info("Reading %s from result", k)
                    if k == name_par:
                        last[name_par] = v
                # logging.info("Writing in  %s the value %s", name_par, last[name_par])
                last[name_par] = float(last[name_par])

        # Write output.
        output_path = Path(dir)
        # logging.info("Writing to %s", output_path)
        with open(output_path, "a+", newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_NONE)
            writer.writerow(last.values())


if __name__ == "__main__":
    main()

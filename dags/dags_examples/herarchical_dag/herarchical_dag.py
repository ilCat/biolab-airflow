from numpy import trunc, ceil
import airflow
from airflow.models.dag import DAG
from airflow.operators.subdag import SubDagOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from scripts.lib.biolab_functions import calc_times
from scripts.lib.biolab_nodes import BiolabNodes
from scripts.lib.biolab_functions import read_config

dag_name = "herarchical_dag"
# ------------------------------------- INITIAL DAG ---------------------------------------------------
with DAG(
        dag_id=dag_name,
        description="First approach  to mix airflow and pyfoomb using Docker. Automatized Example",
        start_date=airflow.utils.dates.days_ago(1),
        schedule_interval="@once",
) as dag:
    # instance of the class with the path of the project. You must change the path for your own path
    Experiment = BiolabNodes(path="C:/Users/Fede/Documents/Airflow/biolab-airflow")

    # -------------- Initialize parameters, initial values and times form config.cfg file --------------
    mbrs_row = ["mbr{}".format(row) for row in range(1, read_config('MBR', dag_name)['QTY_R'] + 1)]
    mbrs_col = ["{}".format(col) for col in range(1, read_config('MBR', dag_name)['QTY_C'] + 1)]
    unknows_estimate = read_config('PARAMETER_ESTIMATION_GUIDE', dag_name)
    unknows_optimization = read_config('PARAMETER_OPTIMIZATION_BOUNDS', dag_name)

    # Set the seed for the entire experiment
    seed = read_config('SEED', dag_name)
    Experiment.set_seed(seed)

    # extract modify params
    modify_params = read_config('MODIFY_PARAMS', dag_name)

    # get sample times
    _, _t_sample = calc_times(dag_name)

    # get hamilton data
    hamilton_plate_size = read_config('HAMILTON', dag_name)['PLATE_SIZE']

    # create bioreactor name row + column
    mbrs = []
    for i in mbrs_row:
        for j in mbrs_col:
            mbrs.append(i + j)

    # ---------------------------------- DAGs Start Node -----------------------------------------
    start = Experiment.start(
        task_id="Start",
        dag_id=dag_name
    )

    # init node (blocks) dictionary, hamilton plate samples and variable for las computational block
    node = {}
    hamilton_samples = []
    last_computational_block = None

    # for each sample defined, do an init/update - sample - parameter estimation - parameter optimization LOOP
    for sample, t_s in enumerate(_t_sample):

        if sample == 0:
            node["Init_MBRs{}".format(sample)] = Experiment.block_init(
                mbrs=mbrs,
                dag_id=dag_name
            )

        # calculate number of column for each sample - Sequential sample C1,C2,C3...
        # the following line generate an index of which column it collects samples. Remind "sample" starts from 0
        column_to_sample = int(sample - len(mbrs_col) * trunc(sample / len(mbrs_col)))

        # create a list of params modifiers from a specific column
        modify_params_col = [item[column_to_sample] for item in modify_params]

        # create a list of bioreactor from a specific column
        mbr_col_list = [i + mbrs_col[column_to_sample] for i in mbrs_row]

        # ------------------------------ block 0 --------------------------------------------------------
        node["Block0_Column{}_T{}".format(column_to_sample + 1, sample + 1)] = Experiment.block0(
            mbrs_row=mbr_col_list,
            instance=sample + 1,
            column_to_sample=column_to_sample
        )

        # ------------------------------ block 1 --------------------------------------------------------
        node["Block1_Column{}_T{}".format(column_to_sample + 1, sample + 1)] = Experiment.block1(
            mbrs_row=mbr_col_list,
            instance=sample + 1,
            column_to_sample=column_to_sample,
            modify_params=modify_params_col,
            tf="{}".format(int(t_s))
        )

        # add sample node_id to hamilton plate
        hamilton_samples.append("Block1_Column{}_T{}".format(column_to_sample + 1, sample + 1))

        # ------------------------------------Dependencies------------------------------------------------
        if sample == 0:
            start >> node["Init_MBRs{}".format(sample)] >> \
            node["Block0_Column{}_T{}".format(column_to_sample + 1, sample + 1)] >> \
            node["Block1_Column{}_T{}".format(column_to_sample + 1, sample + 1)]
        else:
            last_block >> node["Block0_Column{}_T{}".format(column_to_sample + 1, sample + 1)] >> \
            node["Block1_Column{}_T{}".format(column_to_sample + 1, sample + 1)]

        # set the last node
        last_block = node["Block1_Column{}_T{}".format(column_to_sample + 1, sample + 1)]

        # --------------------------- Hamilton and Computational Blocks ----------------------------------

        # check samples number to fill the hamilton plate and send to atline analysis
        # TODO: check remaining samples, where hamilton is not filled
        if len(hamilton_samples) == hamilton_plate_size:
            plate_number = int(sample / hamilton_plate_size)
            block_hamilton = Experiment.block_hamilton(instance=sample + 1)

            # add computational block
            block_computational = Experiment.block_computational(
                mbrs=mbrs,
                instance=sample + 1,
                unknows_estimation=unknows_estimate,
                unknows_optimization=unknows_optimization,
                modify_params=modify_params,
                tf=_t_sample[-1]
            )

            # add dependency between sample block and hamilton
            for node_id in hamilton_samples:
                node[node_id] >> block_hamilton

            # add dependency between hamilton and computational block
            # TODO: add sensor between hamilton and computational (when sample analysis it is done)
            block_hamilton >> block_computational

            # add dependency between computational blocks
            if last_computational_block:
                last_computational_block >> block_computational

            # update last computational block
            last_computational_block = block_computational

            # reset hamilton samples
            hamilton_samples = []

    # ------------------------ Show results  -----------------------------------------------
    # Plot simulation with new policy to compare results. Plot params distribution
    show_results = Experiment.show_results(
        task_id="Plot_Results",
        mbrs=mbrs,
        unknows=unknows_estimate,
        tf="{}".format(int(_t_sample[-1]))
    )

    last_computational_block >> show_results

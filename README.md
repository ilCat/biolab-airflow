# Experimental-Computational Workflows

First approach to the management of experimental-computational workflows with Apache Airflow, Assimulo and Docker.

## Requeriments

* [GIT:](https://git-scm.com/) to download and manage the source code.
* [Docker:](https://docs.docker.com/get-docker/) to manage the dependencies and be able to run this example.

## Frameworks and Libraries used

* [Airflow](https://airflow.apache.org/)
* [Assimulo](https://jmodelica.org/assimulo/)
* [Pytorch](https://pytorch.org/)
* [Pyro](https://pyro.ai/)
* [Bayesian Optimization](https://github.com/fmfn/BayesianOptimization)
* [pyFOOMB](https://github.com/MicroPhen/pyFOOMB)
* [Numpy](https://numpy.org/)

These dependencies are installed automatically by Docker through the definition of the `docker-compose.yml` file and the different images created.

## Contents

This repository contains:

* Different Docker images:
    * `airflow-docker`
    * `assimulo-docker`
    * `nginx-docker`
    * `pyfoomb-docker`
* Dags:
  * Folder with DAGs examples. The most important and complete one it is `herarchical_dag`. This DAG uses the nodes available in the `biolab_nodes.py` file and 
    helper functions defined in the `biolab_functions.py`. It also uses the `config.json` file to configure and create all nodes and dependencies 
    regarding the workflow. 
  * Data: contains the result of the workflow execution. This folder has two types of results:
    * params: results related to the parameter estimation process in each step stored in JSON format.
    * samples: results related to the samples of each MBR stored in JSON format.
  * Plots: charts related to the parameter estimation process in each step, and measures of the model in-silico versus the execution with estimated parameters.
  * Scripts folder with:
    * lib:
        * `biolab_nodes.py`: file with all nodes (and blocks) available in the bioprocess workflow, including experimental and computational tasks. 
        * `biolab_functions.py`: file with helper functions to manage configurations, measures, parameters and input/output from JSON file.
    * models:
      * assimulo:
        * `model_execution.py`: file with Assimulo model to run a simulation from _t0_ to _tf_ with _initial conditions_ defined.
      * pyfoomb (not recomended):
        * `model_execution.py`: file with pyFOOMB model to run a simulation from _t0_ to _tf_ with _initial conditions_ defined.
    * parameter_update:
      * `variational_inference.py`: file with Pyro framework to use Stochastic Variational Inference (SVI), to update the parameters distribution with the use of probabilistic models.
      * `parameter_update.py`: file with PyFOOMB parameter estimation process
    * redesign_experiment:
      * `bayesian_online_redesign.py`: file with Bayesian Optimization process, to redesign the process in an online mode.
      * `bayesian_offline_redesign.py`: file with Bayesian Optimization process, to redesign the process in an offline mode (a pre-process).
    * plotter:
      * `show_results.py`: file with Python code to save plots in the /plot folder. This plots are about measures, comparing the police learned and the in-silico values, and in other hand, the parameters distribution over the steps defined.

## Usage

First, install the tools required.

Then, clone the repository with the use of GIT:

    git clone https://git.tu-berlin.de/bvt-htbd/kiwi/tf2/experimental-computational-workflows

To use this example (herarchical_dag) it is necessary to change the absolute path declaration in the main file `herarchical_dag.py`. This is, instead:

    Experiment=BiolabNodes(path="C:/Users/Username/Documents/Airflow/biolab-airflow")

You must use:

    Experiment=BiolabNodes(path="your/own/full/path/repo_name")

In the same location of `docker_compose.yml` run the Airflow Platform from the terminal using:

    docker-compose up -d

Wait for a few seconds and should be able to access to the DAGs examples at _http://localhost:8080/_.

Log in with _user: admin_ and _password: admin_.

Here, there are the examples `herarchical_dag`, `automatized_dag` and `simple_dag`. Activate the example with the button on the left, and press the play icon to run it.

To stop running the Platform, enter the following command in the terminal:

    docker-compose down -v

## FAIR Principles

In the `config.json` file, it is possible to change the seed to reproduce another results on the samples generated. In this case, the string "12345678" is taken and is arbitrarily selected.

## Customization

In this case, all nodes uses the Assimulo framework image. If another framework is required, a new image must be created with its own
dependencies and also modify the existing nodes or create new ones.

## Wiki

For more information about the project, please visit the [WIKI](https://git.tu-berlin.de/bvt-htbd/kiwi/tf2/experimental-computational-workflows/-/wikis/home) section.

## Credits

This project was made by Federico Mione<sup>*</sup>, Alexis Silva and Prof. Dr. Ernesto Martínez.

<sup>*</sup>Email contact: fmione@santafe-conicet.gov.ar


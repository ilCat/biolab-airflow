import airflow
from airflow import DAG
from scripts.lib.biolab_functions import read_config
from scripts.lib.biolab_nodes import BiolabNodes

dag_name = "probabilistic_model_dag"
with DAG(
        dag_id=dag_name,
        description="First approach  to mix airflow and pyfoomb using Docker. Simple Example.",
        start_date=airflow.utils.dates.days_ago(1),
        schedule_interval=None,
) as dag:
    # Instance of the class with the path of the project. You must change the path for your own path
    Experiment = BiolabNodes(path="C:/Users/Fede/Documents/Airflow/biolab-airflow")

    # --------------------------------------- DAGs Start Node -----------------------------------------------------
    start = Experiment.start(
        task_id="Start",
        dag_id=dag_name
    )
    # -------------------------------- Initialice  initial values form config.cfg ---------------------------------
    init_mbr = Experiment.init_mbr(
        task_id="Init_MBR",
        mbr="mbr",
        dag_id=dag_name
    )
    # ------------------------------ Real model Samples and prior model simulation   ------------------------------
    sample_1 = Experiment.sample(
        task_id="Sample_1",
        modify_param={'mu_set': 1.01},
        tf="3.0",
        mbr="mbr"
    )
    sample_2 = Experiment.sample(
        task_id="Sample_2",
        modify_param={'mu_set': 1.01},
        tf="6.0",
        mbr="mbr"
    )
    # ---------------------------------------- Parameter estimation -----------------------------------------------
    parameter_update_1 = Experiment.parameter_distribution_update(
        task_id="Parameter_Distribution_Update",
        mbrs=["mbr"],
        unknows={},
        modify_params={}
    )
    # -------------------------------------- Parameter optimization -----------------------------------------------
    unknows_opt = read_config("PARAMETER_OPTIMIZATION_BOUNDS", dag_name)
    online_redesign_1 = Experiment.bayesian_online_redesign(
        task_id="Online_Redesign_1",
        mbrs=["mbr"],
        unknows=unknows_opt,
        tf="7.0"
    )
    # ------------------------ Real model Samples and prior model simulation   -------------------------------------
    sample_3 = Experiment.sample(
        task_id="Sample_3",
        modify_param={'mu_set': 1.01},
        tf="10.0",
        mbr="mbr"
    )
    sample_4 = Experiment.sample(
        task_id="Sample_4",
        modify_param={'mu_set': 1.01},
        tf="14.0",
        mbr="mbr"
    )

    # ---------------------------------------- Parameter estimation -----------------------------------------------
    parameter_update_2 = Experiment.parameter_distribution_update(
        task_id="Parameter_Distribution_Update_2",
        mbrs=["mbr"],
        unknows={},
        modify_params={}
    )
    # -------------------------------------- Parameter optimization -----------------------------------------------
    online_redesign_2 = Experiment.bayesian_online_redesign(
        task_id="Online_Redesign_2",
        mbrs=["mbr"],
        unknows=unknows_opt,
        tf="15.0"
    )
    # ------------------------ Real model Samples and prior model simulation   -------------------------------------
    sample_5 = Experiment.sample(
        task_id="Sample_5",
        modify_param={'mu_set': 1.01},
        tf="18.0",
        mbr="mbr"
    )

    # ---------------------------------------- Parameter estimation -----------------------------------------------
    parameter_update_3 = Experiment.parameter_distribution_update(
        task_id="Parameter_Distribution_Update_3",
        mbrs=["mbr"],
        unknows={},
        modify_params={}
    )
    # -------------------------------------- Parameter optimization -----------------------------------------------
    online_redesign_3 = Experiment.bayesian_online_redesign(
        task_id="Online_Redesign_3",
        mbrs=["mbr"],
        unknows=unknows_opt,
        tf="19.0"
    )
    # ------------------------ Real model Samples and prior model simulation   -------------------------------------
    sample_6 = Experiment.sample(
        task_id="Sample_6",
        modify_param={'mu_set': 1.01},
        tf="22.0",
        mbr="mbr"
    )
    # ------------------------ Real model Samples and prior model simulation   -------------------------------------
    sample_7 = Experiment.sample(
        task_id="Sample_7",
        modify_param={'mu_set': 1.01},
        tf="30.0",
        mbr="mbr"
    )
    # --------------------------------------------Dependencies-----------------------------------------------------
    start >> init_mbr >> sample_1 >> sample_2
    sample_2 >> parameter_update_1 >> online_redesign_1 >> sample_3 >> sample_4
    sample_4 >> parameter_update_2 >> online_redesign_2 >> sample_5
    sample_5 >> parameter_update_3 >> online_redesign_3 >> sample_6 >> sample_7

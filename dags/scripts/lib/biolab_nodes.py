import logging
import json
import sys

# to import the library biolab_functions
sys.path.insert(0, '../../..')
from scripts.lib.biolab_functions import init_params, init_measurements
from airflow.providers.docker.operators.docker import DockerOperator
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from airflow.utils.task_group import TaskGroup

logging.basicConfig(level=logging.INFO)


class BiolabNodes:

    def __init__(self, path):
        self.output_dir = path
        self.seed = "12345678"
        self.volumes = ["{}/dags/data:/data".format(self.output_dir),
                        "{}/dags/scripts:/scripts".format(self.output_dir),
                        "{}/dags/plots:/plots".format(self.output_dir)]

    def set_seed(self, seed):
        self.seed = seed

    def dummy(self, task_id):
        return DummyOperator(task_id=task_id)

    def start(self, task_id, dag_id):
        return PythonOperator(
            task_id=task_id,
            python_callable=init_params,
            op_kwargs={'dag_id': dag_id},
        )

    def init_mbr(self, task_id, mbr, dag_id):
        return PythonOperator(
            task_id=task_id,
            python_callable=init_measurements,
            op_kwargs={'mbr': mbr, 'dag_id': dag_id},
        )

    def sample(self, task_id, modify_param, tf, mbr):
        return DockerOperator(
            task_id=task_id,
            image="assimulo-docker",
            command=["python", "/scripts/models/assimulo/model_execution.py", "--tf", tf, "--mbr", mbr,
                     "--modify", json.dumps(modify_param), "--seed", self.seed],
            volumes=self.volumes,
            network_mode="airflow",
            auto_remove=True
        )

    def parameter_update(self, task_id, mbrs, unknows):
        return DockerOperator(
            task_id=task_id,
            image="pyfoomb-docker",
            command=["python", "/scripts/parameter_update/parameter_update.py", "--mbrs", json.dumps(mbrs), "--unknows",
                     json.dumps(unknows), "--seed", self.seed],
            volumes=self.volumes,
            network_mode="airflow",
            auto_remove=True
        )

    def parameter_distribution_update(self, task_id, mbrs, unknows, modify_params):
        return DockerOperator(
            task_id=task_id,
            image="assimulo-docker",
            command=["python", "/scripts/parameter_update/variational_inference.py", "--mbrs", json.dumps(mbrs),
                     "--unknows", json.dumps(unknows), "--modify", json.dumps(modify_params), "--seed", self.seed],
            volumes=self.volumes,
            network_mode="airflow",
            auto_remove=True
        )

    def online_redesign(self, task_id, mbrs, unknows, tf):
        return DockerOperator(
            task_id=task_id,
            image="pyfoomb-docker",
            command=["python", "/scripts/redesign_experiment/online_redesign.py", "--mbrs", json.dumps(mbrs),
                     "--unknows", json.dumps(unknows), "--tf", tf, "--seed", self.seed],
            volumes=self.volumes,
            network_mode="airflow",
            auto_remove=True
        )

    def bayesian_online_redesign(self, task_id, mbrs, unknows, tf):
        return DockerOperator(
            task_id=task_id,
            image="assimulo-docker",
            command=["python", "/scripts/redesign_experiment/bayesian_online_redesign.py", "--mbrs", json.dumps(mbrs),
                     "--unknows", json.dumps(unknows), "--tf", tf, "--seed", self.seed],
            volumes=self.volumes,
            network_mode="airflow",
            auto_remove=True
        )

    def bayesian_offline_redesign(self, task_id):
        return DockerOperator(
            task_id=task_id,
            image="pyfoomb-bayesian-docker",
            command=["python", "/scripts/redesign_experiment/bayesian_offline_redesign.py", "--seed", self.seed],
            volumes=self.volumes,
            network_mode="airflow",
            auto_remove=True
        )

    def show_results(self, task_id, mbrs, tf, unknows):
        return DockerOperator(
            task_id=task_id,
            image="assimulo-docker",
            command=["python", "/scripts/plotter/show_results.py", "--tf", tf, "--mbrs", json.dumps(mbrs), "--unknows",
                     json.dumps(unknows), "--seed", self.seed],
            volumes=self.volumes,
            network_mode="airflow",
            auto_remove=True
        )

    def call_hamilton(self, task_id):
        return DummyOperator(
            task_id=task_id
        )

    def at_line_analysis(self, task_id):
        return DummyOperator(
            task_id=task_id,
        )

    # --------------------------------Blocks of tasks ---------------------------------------

    def block_init(self, mbrs, dag_id):
        with TaskGroup("Init_MBRs", tooltip="Tasks for Block_init_MBRs") as block_init:
            for mbr in mbrs:
                PythonOperator(
                    task_id="Init_{}".format(mbr),
                    python_callable=init_measurements,
                    op_kwargs={'mbr': mbr, 'dag_id': dag_id},
                )
        return block_init

    def block0(self, column_to_sample, instance, mbrs_row):
        with TaskGroup("Block0_Column{}_T{}".format(column_to_sample + 1, instance),
                       tooltip="Tasks for Block0_{}".format(instance)) as block_0:
            for i in range(len(mbrs_row)):
                clean_needles = self.dummy(
                    task_id=f"Clean_Needles_Row{i + 1}",
                )
                volume_balance = self.dummy(
                    task_id=f"Volume_Balance_Row{i + 1}",
                )
                ph_control_1 = self.dummy(
                    task_id=f"PH_Control_1_Row{i + 1}",
                )
                feed = self.dummy(
                    task_id=f"Feed_Row{i + 1}",
                )
                ph_control_2 = self.dummy(
                    task_id=f"PH_Control_2_Row{i + 1}",
                )
                ph_control_3 = self.dummy(
                    task_id=f"PH_Control_3_Row{i + 1}",
                )
                ph_control_4 = self.dummy(
                    task_id=f"PH_Control_4_Row{i + 1}",
                )
                ph_control_5 = self.dummy(
                    task_id=f"PH_Control_5_Row{i + 1}",
                )
                clean_needles >> volume_balance >> ph_control_1 >> feed >> \
                ph_control_2 >> ph_control_3 >> ph_control_4 >> ph_control_5
        return block_0

    def block1(self, column_to_sample, instance, mbrs_row, modify_params, tf):
        with TaskGroup("Block1_Column{}_T{}".format(column_to_sample + 1, instance),
                       tooltip="Tasks for Block_1_{}".format(instance)) as block_1:
            for i in range(len(mbrs_row)):
                clean_needles_1 = self.dummy(
                    task_id=f"Clean_Needles_1_Row{i + 1}",
                )
                volume_balance = self.dummy(
                    task_id=f"Volume_Balance_Row{i + 1}",
                )
                ph_control = self.dummy(
                    task_id=f"PH_Control_Row{i + 1}",
                )
                feed = self.dummy(
                    task_id=f"Feed_Row{i + 1}",
                )
                clean_needles_2 = self.dummy(
                    task_id=f"Clean_Needles_2_Row{i + 1}",
                )
                at_line_sampling = self.sample(
                    task_id=f"At_Line_Sampling_Row{i + 1}",
                    modify_param=modify_params[i],
                    tf=tf,
                    mbr=mbrs_row[i]
                )
                clean_needles_1 >> volume_balance >> ph_control >> feed >> clean_needles_2 >> at_line_sampling
        return block_1

    def block_computational(self, instance, mbrs, unknows_estimation, unknows_optimization, modify_params, tf):
        with TaskGroup("Block_Computational_T{}".format(instance),
                       tooltip="Tasks for Block_Computational at sample time number {}".format(
                           instance)) as block_computational:
            parameter_distribution_update = self.parameter_distribution_update(
                task_id="Parameter_Distribution_Update",
                mbrs=mbrs,
                unknows=unknows_estimation,
                modify_params=modify_params
            )
            # online_redesign = self.bayesian_online_redesign(
            #     task_id="Online_Redesign",
            #     mbrs=mbrs,
            #     unknows=unknows_optimization,
            #     tf=str(tf)
            # )
            # parameter_distribution_update >> online_redesign
        return block_computational

    def block_hamilton(self, instance):
        with TaskGroup("Block_Hamilton_T{}".format(instance),
                       tooltip="Tasks for Block_Hamilton at sample time number {}".format(instance)) as block_hamilton:
            call_hamilton = self.call_hamilton(
                task_id="Call_Hamilton"
            )
            at_line_analysis = self.at_line_analysis(
                task_id="At_Line_Analysis"
            )
            call_hamilton >> at_line_analysis

        return block_hamilton

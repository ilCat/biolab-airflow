version: "3.7"
# ====================================== AIRFLOW ENVIRONMENT VARIABLES =======================================
x-environment: &airflow_environment
  # Airflow settings.
  - AIRFLOW__CORE__EXECUTOR=LocalExecutor
  - AIRFLOW__CORE__LOAD_DEFAULT_CONNECTIONS=False
  - AIRFLOW__CORE__LOAD_EXAMPLES=False
  - AIRFLOW__CORE__SQL_ALCHEMY_CONN=postgresql://airflow:airflow@postgres:5432/airflow
  - AIRFLOW__CORE__STORE_DAG_CODE=True
  - AIRFLOW__CORE__STORE_SERIALIZED_DAGS=True
  - AIRFLOW__WEBSERVER__EXPOSE_CONFIG=True
  - AIRFLOW__WEBSERVER__RBAC=False
  - AIRFLOW__CORE__ENABLE_XCOM_PICKLING= true

x-airflow-image: &airflow_image apache/airflow:2.0.0-python3.8
# ====================================== /AIRFLOW ENVIRONMENT VARIABLES ======================================
services:
  postgres:
    image: postgres:12-alpine
    container_name: docker_postgres
    environment:
      - POSTGRES_USER=airflow
      - POSTGRES_PASSWORD=airflow
      - POSTGRES_DB=airflow
    ports:
      - "5432:5432"
    networks:
      - airflow
  init:
    build:
      context: images/airflow-docker
      args:
        AIRFLOW_BASE_IMAGE: *airflow_image
    image: airflow-docker
    container_name: docker_init
    depends_on:
      - postgres
    networks:
      - airflow
    environment: *airflow_environment
    entrypoint: /bin/bash
    command: -c 'airflow db upgrade && sleep 5 && airflow users create --username admin --password admin --firstname Anonymous --lastname Admin --role Admin --email admin@example.org'
  webserver:
    build:
      context: images/airflow-docker
      args:
        AIRFLOW_BASE_IMAGE: *airflow_image
    image: airflow-docker
    container_name: docker_webserver
    restart: always
    depends_on:
      - postgres
    ports:
      - "8080:8080"
    volumes:
      - logs:/opt/airflow/logs
    networks:
      - airflow
    environment: *airflow_environment
    command: webserver
  scheduler:
    build:
      context: images/airflow-docker
      args:
        AIRFLOW_BASE_IMAGE: *airflow_image
    image: airflow-docker
    container_name: docker_scheduler
    restart: always
    depends_on:
      - postgres
    volumes:
      - ./dags:/opt/airflow/dags
      - logs:/opt/airflow/logs
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - airflow
    environment: *airflow_environment
    command: scheduler


  # These aren't actual services, but we include them to make sure that the
  # corresponding images are built by when pulling up docker-compose.
  pyfommb-docker:
    build: images/pyfoomb-docker
    image: pyfoomb-docker
    container_name: pyfoombdocker
    restart: "no"
    volumes:
      - ./dags:/opt/airflow/dags
    networks:
      - airflow

  assimulo-docker:
    build: images/assimulo-docker
    image: assimulo-docker
    container_name: assimulodocker
    restart: "no"
    volumes:
      - ./dags:/opt/airflow/dags
    networks:
      - airflow

  grafana-docker:
    image: grafana/grafana
    container_name: grafanadocker
    restart: "no"
    ports:
      - "3000:3000"
    volumes:
      - ./dags:/usr/share/grafana/dags
    networks:
      - airflow

  nginx-docker:
    build: images/nginx-docker
    image: nginx-docker
    container_name: nginxdocker
    restart: "no"
    ports:
      - "8082:8082"
    volumes:
      - ./dags:/etc/nginx/html/dags
    networks:
      - airflow


networks:
  airflow:
    name: airflow

volumes:
  logs:
    name: docker_logs

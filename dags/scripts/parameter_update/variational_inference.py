import sys
import json
import logging
import numpy as np
import click
import torch
import torch.distributions.constraints as constraints
import pyro
from pyro.optim import Adam
from pyro.infer import SVI, Trace_ELBO
import pyro.distributions as dist

# to import the library biolab_functions and model_execution
sys.path.insert(0, '../../..')
from scripts.models.assimulo.model_execution import ExponentialFedBatch
from scripts.lib.biolab_functions import get_params, get_measurements, save_params


class VariationalInference:

    def __init__(self, model=None, prior=None, guide=None, modify_params=None):
        pyro.clear_param_store()

        self.simulated_model = model

        # guide params fixed only for distribution params
        self.distribution_guide = guide

        # init prior params
        self.params_distribution = prior

        # init modify_param for each mbr
        self.modify_params = modify_params

    def model(self, data):
        # sample theta[i] from prior distribution if it is probabilistic
        theta = self.sample_param()

        # check data and compare error (len(data) == MBR_QTY)
        for i, mbr_sample in enumerate(data):

            # simulate from sample [j-1] to sample[j]
            for j, samples in enumerate(mbr_sample):
                if j != 0:
                    # get data from MBR "i" at sample "j"
                    x0 = [v for k, v in mbr_sample[j - 1].items() if k != 'TP']
                    initial_values = {f'{k}': v for k, v in mbr_sample[0].items() if k != 'TP'}

                    # -------------------------- logging --------------------------
                    logging.info(x0)
                    logging.info(initial_values)
                    logging.info(theta)
                    logging.info([mbr_sample[j-1]['TP'], mbr_sample[j]['TP']])

                    try:
                        # Modify params (apply coeff)
                        for param in self.modify_params[i]:
                            theta[param] = theta[param] * self.modify_params[i][param]

                        obs = self.simulated_model.execute(x0=x0, t=[mbr_sample[j - 1]['TP'], mbr_sample[j]['TP']],
                                                           model_parameters=theta, initial_values=initial_values)

                        # real sample value stored, without 'TP'
                        y = [v for k, v in mbr_sample[j].items() if k != 'TP']

                        # add noise to the observed value with normal distribution
                        for k in range(len(obs[1][-1])):
                            pyro.sample("obs_{}_{}_{}".format(i, j, k), dist.Normal(obs[1][-1][k], 0.0002),
                                        obs=torch.tensor(y[k]))
                    except:
                        logging.info("[Variational-Inference] - Error on model execution - continue")
                        continue

    def guide(self, data):
        # register two variational params (for each theta) in Pyro (alpha, beta)
        for param in self.distribution_guide.keys():
            alpha_q = pyro.param("alpha_q_{}".format(param), torch.tensor(self.distribution_guide[param][0]),
                                 constraint=constraints.positive)
            beta_q = pyro.param("beta_q_{}".format(param), torch.tensor(self.distribution_guide[param][1]),
                                constraint=constraints.positive)
            # sample theta
            pyro.sample("theta_{}".format(param), dist.Beta(alpha_q, beta_q))

    def sample_param(self):
        theta = {}
        # sample theta[i] from prior distribution if it is probabilistic
        for param in self.params_distribution.keys():
            if isinstance(self.params_distribution[param], list):
                # probabilistic
                theta[param] = pyro.sample("theta_{}".format(param),
                                           dist.Beta(self.params_distribution[param][0],
                                                     self.params_distribution[param][1])).item()

            else:
                # deterministic
                theta[param] = self.params_distribution[param]
        return theta

    def optimize(self, data, n_steps, lr=0.0005, betas=(0.90, 0.999)):
        # set the optimizer
        optimizer = Adam({"lr": lr, "betas": betas})

        # set inference algorithm
        svi = SVI(self.model, self.guide, optimizer, loss=Trace_ELBO())

        # loop from step
        for step in range(n_steps):
            svi.step(data)
            if step % 100 == 0:
                print('.', end='')

        # return learned values
        distributions = self.params_distribution.copy()
        for param in self.distribution_guide.keys():
            alpha_q = round(pyro.param("alpha_q_{}".format(param)).item(), 3)
            beta_q = round(pyro.param("beta_q_{}".format(param)).item(), 3)
            distributions[param] = [alpha_q, beta_q]

        return distributions


@click.command()
@click.option(
    "--mbrs",
    type=click.STRING,
    required=True,
    help="Minibioreactor list",
)
@click.option(
    "--unknows",
    type=click.STRING,
    required=True,
    help="Parameter to optimize and bounds",
)
@click.option(
    "--modify",
    type=click.STRING,
    required=True,
    help="Parameter modify factor for each mbr",
)
@click.option(
    "--seed",
    type=click.INT,
    required=False,
    help="Seed for random number generator",
)
def main(mbrs, unknows, modify, seed):
    # set seed for torch and numpy frameworks
    torch.manual_seed(seed=seed)
    np.random.seed(seed=seed)

    # ---------------------------------- GET DATA ---------------------------------------------------------

    MBRs = json.loads(mbrs)
    _unknows = json.loads(unknows)
    modify_params = np.array(json.loads(modify)).flatten()

    model_parameters = get_params()
    data = []
    model = ExponentialFedBatch()

    for mbr in MBRs:
        mbr_measurements = get_measurements(mbr)
        data.append(mbr_measurements['samples'])

    # ---------------------------------- LOGGING ---------------------------------------------------------

    logging.info("mbrs:")
    logging.info(MBRs)
    logging.info("unknows:")
    logging.info(_unknows)
    logging.info("prior parameters:")
    logging.info(model_parameters['parameters_estimated'][-1])
    logging.info("measures:")
    logging.info(data)

    # ---------------------------------- INFERENCE --------------------------------------------------------

    logging.info("Running SVI...")
    svi = VariationalInference(model=model, prior=model_parameters['parameters_estimated'][-1], guide=_unknows,
                               modify_params=modify_params)
    distributions = svi.optimize(data, 2000)
    logging.info("Estimation Finished")

    # --------------------------------- SAVE RESULTS ------------------------------------------------------

    # add parameters distributions re-estimated
    model_parameters['parameters_estimated'].append(distributions)

    # save new params
    save_params(model_parameters)

    return distributions


if __name__ == "__main__":
    main()

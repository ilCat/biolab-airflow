import logging
import click
import sys
import json
import numpy as np
from scipy import signal
from assimulo.problem import Explicit_Problem
from assimulo.solvers.sundials import CVode

# to import the library biolab_functions
sys.path.insert(0, '../../..')
from scripts.lib.biolab_functions import get_params, get_measurements, save_measurements, read_config

logging.basicConfig(level=logging.INFO)


# Defines the model class
class ExponentialFedBatch(Explicit_Problem):

    def rhs(self, t, y):

        # Unpacks the state vector. The states are alphabetically ordered.
        P, S, VL, X = y

        # Unpacks the model parameters.
        YPX = self.model_parameters['YPX']
        YXS = self.model_parameters['YXS']
        cSF = self.model_parameters['cSF']
        mu_set = self.model_parameters['mu_set']
        tF = self.model_parameters['tF']
        VL_max = self.model_parameters['VL_max']

        # For calculation of F, these two initial values are needed
        S0 = self.initial_values['S']
        VL0 = self.initial_values['VL']

        # Calculate the current specific rates
        mu = self.growth_rate(y)
        qS = 1 / YXS * mu
        qP = YPX * mu

        # Calculate the feeding profile, conditional to the corresponding events
        if t > tF and not VL > VL_max:
            # TODO: check square with sample number (if increase this value it is wrong)
            # square = (1 + signal.square(2 * np.pi / 5 * (t - tF), 0.1)) / 2
            square = 1
            # square = (2 + signal.square(2 * np.pi / 5 * (t - tF), 0.1))
            profile = (S0 * VL0 * mu_set) / (cSF - S) * np.exp(mu_set * (t - tF))
            F = square * profile
        else:
            F = 0.0

        # Calculate state derivatives
        dXdt = mu * X - F / VL * X
        dSdt = -qS * X + F / VL * (cSF - S)
        dPdt = qP * X - F / VL * P
        dVLdt = F

        # Return list of state derivatives in the same order as the state vector was unpacked
        return [dPdt, dSdt, dVLdt, dXdt]

    # The Monod equation is defined as instance method
    def growth_rate(self, y):
        P, S, VL, X = y
        mu_max = self.model_parameters['mu_max']
        kS = self.model_parameters['kS']
        mu = mu_max * S / (kS + S)
        return mu

    def execute(self, x0, t, model_parameters, initial_values):
        # actualiza valores de parametros y acciones para la ejecución actual
        self.t0 = t[0]
        self.y0 = x0
        self.model_parameters = model_parameters
        self.initial_values = initial_values

        simulator = CVode(self)
        try:
            result = simulator.simulate(tfinal=t[1], ncp=30)
        except:
            result = simulator.simulate(tfinal=t[1])
        return result


@click.command()
@click.option(
    "--mbr",
    type=click.STRING,
    required=True,
    help="Initial values",
)
@click.option(
    "--modify",
    type=click.STRING,
    required=True,
    help="Parameters modifier",
)
@click.option(
    "--tf",
    type=click.FLOAT,
    required=True,
    help="Final execution time",
)
@click.option(
    "--seed",
    type=click.INT,
    required=False,
    help="Seed for random number generator",
)
def main(mbr, modify, tf, seed):
    # Set seed for random number generator
    np.random.seed(seed)

    # ---------------------------------- GET DATA ---------------------------------------------------------

    logging.info("Initializing bioreactor parameters")
    model_parameters = get_params()
    measurements = get_measurements(mbr)
    measurements_name = measurements['samples'][0].keys()
    modify_param = json.loads(modify)

    t0 = measurements['samples'][-1]['TP']

    # Modify params (apply coeff)
    for param in modify_param:
        model_parameters['parameters_real'][param] = model_parameters['parameters_real'][param] * modify_param[param]

    x0 = [measure for key, measure in measurements['samples'][-1].items() if key != 'TP']

    # ---------------------------------- LOGGING ---------------------------------------------------------

    logging.info("modify coefficient:  %s", modify_param)
    logging.info("t0:  %s", t0)
    logging.info("tf:  %s", tf)
    logging.info("mbr:  %s", mbr)
    logging.info("initial measures:")
    logging.info(measurements['samples'][-1])
    logging.info("model parameters updated:")
    logging.info(model_parameters['parameters_real'])

    # ---------------------------------- SIMULATION ------------------------------------------------------

    logging.info("Running real simulation...")
    results_real = ExponentialFedBatch().execute(x0=x0, t=[t0, tf],
                                                 model_parameters=model_parameters['parameters_real'],
                                                 initial_values=measurements['samples'][0])
    logging.info("Simulation finished")

    # --------------------------------- SAVE RESULTS ------------------------------------------------------

    # add noise (scale * 0.025) and concatenate "tp" with real measurements
    real = np.concatenate(
        (list(map(lambda val: np.abs(np.round(np.random.normal(val, np.abs(val) * 0.025), 4)), results_real[1])),
         np.transpose([results_real[0]])), axis=1)

    # create measurement dictionary and add samples results to json
    measurements['samples'] = list(np.concatenate((measurements['samples'], [dict(zip(measurements_name, real[-1]))])))

    # save measurements
    save_measurements(mbr, measurements)


if __name__ == "__main__":
    main()
